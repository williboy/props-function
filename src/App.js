import React from 'react';
import Maman from './components/Maman';
import './App.css';

function App() {
  return (
    <div className="App">
      <maman />
    </div>
  );
}

export default App;
